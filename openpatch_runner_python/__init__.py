from flask import Flask, jsonify
from flask_cors import cross_origin
from instance.config import app_config
from openpatch_runner_python.api.v1 import api as api_v1


def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])

    # register api endpoints
    app.register_blueprint(api_v1, url_prefix="/v1")

    @app.route("/healthcheck", methods=["GET"])
    @cross_origin()
    def healthcheck():
        return jsonify({"status": "healthy"}), 200

    @app.route("/endpoints")
    @cross_origin()
    def get_endpoints():
        return jsonify({"payload": "/v1/run/post"})

    return app
