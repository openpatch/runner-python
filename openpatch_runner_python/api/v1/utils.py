import os
import subprocess
import time
import re


def convert_unittest_output(raw_unittest_output):
    if len(raw_unittest_output) == 0:
        return {
            "error": "Could not parse unittest-output.",
            "error_msg": "Given unittest_output was either empty, malformed or "
            "the test did not compile at all.",
            "unittest_output": raw_unittest_output,
        }

    if re.search("0 tests", raw_unittest_output):
        return {
            "error": "Could not parse unittest-output.",
            "error_msg": "No tests were provided!",
            "unittest_output": raw_unittest_output,
        }

    if re.search("FAIL", raw_unittest_output):
        report = {"is_correct": False, "values": []}

        total_tests_match = re.search(
            r"Ran (\d+) tests .*\n\nFAILED \(failures=(\d+)\)", raw_unittest_output
        )

        if total_tests_match:
            report["total_tests_run"] = int(total_tests_match.group(1))
            report["total_tests_failed"] = int(total_tests_match.group(2))

            # extract blocks
            blocks = re.findall(r"==*\n([\s\S]*?)\n\n", raw_unittest_output)
            for block in blocks:
                test_match = re.search(r"FAIL: (.*) \((.*)\)", block)
                method_name = test_match.group(1)
                class_name = test_match.group(2)
                module_name = None
                if "." in class_name:
                    module_class = class_name.rsplit(".", 1)
                    module_name = module_class[0]
                    class_name = module_class[1]

                line = int(re.search(r", line (\d+), in", block).group(1))
                error = re.search(r"AssertionError: (.*)", block).group(1)

                report["values"].append(
                    {
                        "method_name": method_name,
                        "class_name": class_name,
                        "module_name": module_name,
                        "line": line,
                        "assertion_error": error,
                    }
                )

        return report

    else:
        report = {
            "is_correct": True,
            "total_tests_failed": 0,
            "values": [],
        }
        total_tests_match = re.search(r"Ran (\d+) tests", raw_unittest_output)

        if total_tests_match:
            report["total_tests_run"] = int(total_tests_match.group(1))

        return report


class PythonToolchainFacade:
    """PythonToolchainFacade (PTF) encapsulates all of python's toolchain."""

    def __init__(self):
        self._base_dir = (
            "/tmp/"
            + "flask_pid-"
            + str(os.getpid())
            + "_"
            + str(round(time.time() * 1000))
            + "/"
        )
        self._sources_dir = self._base_dir
        self._tests_dir = self._base_dir + "tests/"
        self.make_module_dir(self._base_dir)
        self.make_module_dir(self._tests_dir)
        self.make_module_dir(self._sources_dir)

    def make_module_dir(self, path):
        os.makedirs(path, exist_ok=True)
        with open(path + "__init__.py", "a"):
            os.utime(path, None)

    def write_file(self, module, content, sourcetype):
        path = self._base_dir
        if sourcetype == "test":
            path = self._tests_dir
        path = path + module.replace(".", "/") + ".py"
        self.make_module_dir(os.path.dirname(path))
        with open(path, "w") as file:
            file.write(content)

    def run(self, module, sourcetype):
        module = module.replace(".", "/")
        if sourcetype == "source":
            return self.call_subprocess("python3 " + module + ".py", cwd=self._base_dir)
        elif sourcetype == "test":
            return self.call_subprocess(
                "python3 -m unittest tests/" + module + ".py", cwd=self._base_dir
            )

    def compile_py_compile(self, module, sourcetype):
        path = module.replace(".", "/") + ".py"
        if sourcetype == "test":
            path = "tests/" + path
        return self.call_subprocess("python3 -m py_compile " + path, cwd=self._base_dir)

    def call_subprocess(self, call, timeout=None, cwd=None):
        """Calls a subprocess. This is a wrapper method for all toolchain calls.

        :param call: (String) CLI-call to a program, which will be executed.
        :param timeout: (Int) A timeout, after which the execution will be terminated.
        :param cwd: (String) The current working directory in which context the call will be made.
        :return: (dict) with the call, the exitcode, stdout, stderr and the execution time.
        """
        start_time = time.time()

        # Set priority of started commands to 'low' (i.e. nice value of 15)
        # We cannot effectively limit a docker container's resources without imposing strict guidelines
        # (e.g. "use only 10% of cpu time").
        call = "nice -n15 " + call

        cmd = subprocess.Popen(
            [call], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, cwd=cwd
        )

        exit_code = cmd.wait(timeout)

        stop_time = time.time()
        cmd_out, cmd_err = cmd.communicate()

        return {
            "call": call,
            "exitcode": exit_code,
            "stdout": cmd_out.decode("utf-8"),
            "stderr": cmd_err.decode("utf-8"),
            "exectime": stop_time - start_time,
        }
