from flask import jsonify, request
from openpatch_runner_python.api.v1 import api
from openpatch_runner_python.api.v1.utils import (
    PythonToolchainFacade,
    convert_unittest_output,
)


@api.route("/run/post", methods=["POST"])
def run_post():
    """
    @api {post} /v1/run Execute Python-sourcecode.
    @apiVersion 1.0.0
    @apiName ExecutePython
    @apiGroup Runner

    @apiPermission api

    @apiParamExample {json} Example Request to Runner-Python:
    {
        "sources": [
            {
                "module": "my_module.hi",
                "code": "def print_hi():
                            return 'Hi'"
            }
        ],
        "tests": [
            {
                "module": "test_hi",
                "code": "import unittest
                         from my_module.hi import print_hi 
                         
                         class TestHi(unittest.TestCase):
                            def test_print_hi_success(self):
                                self.assertEqual(print_hi(), 'Hi')

                            def test_print_hi_fail(self):
                                self.assertEqual(print_hi(), 'Hu')"
            }
        ]
    }

    @apiSuccess {json} runnerResult Results from compiling and executing Python-sources and -tests.
    @apiSuccessExample {json} Example Success-Response for a runner-python:
    {
        "src_compiles": [
            {
            "call": "nice -n15 python3 -m py_compile my_module/hello_word.py",
            "exectime": 0.2017357349395752,
            "exitcode": 0,
            "stderr": "",
            "stdout": ""
            }
        ],
        "src_runs": [
            {
            "call": "nice -n15 python3 my_module/hello_word.py",
            "exectime": 0.037158966064453125,
            "exitcode": 0,
            "stderr": "",
            "stdout": ""
            }
        ],
        "test_compiles": [
            {
            "call": "nice -n15 python3 -m py_compile tests/test.py",
            "exectime": 0.0678703784942627,
            "exitcode": 0,
            "stderr": "",
            "stdout": ""
            }
        ],
        "test_results": [
            {
                "is_correct": false,
                "total_tests_failed": 1,
                "total_tests_run": 2,
                "values": [
                    {
                    "assertion_error": "'Hi' != 'Hu'",
                    "class_name": "TestHelloWorld",
                    "line": 9,
                    "method_name": "test_print_hi_fail",
                    "module_name": "tests.test"
                    }
                ]
            }
        ],
        "test_runs": [
            {
            "call": "nice -n15 python3 -m unittest tests/test.py",
            "exectime": 0.20090317726135254,
            "exitcode": 1,
            "stderr": ".F\n======================================================================\nFAIL: test_print_hi_fail (tests.test.TestHelloWorld)\n----------------------------------------------------------------------\nTraceback (most recent call last):\n  File \"/tmp/flask_pid-190_1590703795506/tests/test.py\", line 9, in test_print_hi_fail\n    self.assertEqual(print_hi(), 'Hu')\nAssertionError: 'Hi' != 'Hu'\n- Hi\n+ Hu\n\n\n----------------------------------------------------------------------\nRan 2 tests in 0.002s\n\nFAILED (failures=1)\n",
            "stdout": ""
            }
        ]
    }
    """
    ptf = PythonToolchainFacade()
    json = request.get_json()

    try:
        src_compiles = []
        src_runs = []

        for sourcefile in json["sources"]:
            ptf.write_file(sourcefile["module"], sourcefile["code"], "source")
            src_compiles.append(ptf.compile_py_compile(sourcefile["module"], "source"))
            src_runs.append(ptf.run(sourcefile["module"], "source"))

        test_runs = []
        test_compiles = []
        test_results = []

        for testfile in json.get("tests", []):
            module = testfile["module"]
            ptf.write_file(module, testfile["code"], "test")
            test_compiles.append(ptf.compile_py_compile(module, "test"))
            test_runs.append(ptf.run(module, "test"))

        for test_run in test_runs:
            test_results.append(convert_unittest_output(test_run["stderr"]))

        return jsonify(
            {
                "src_compiles": src_compiles,
                "test_compiles": test_compiles,
                "src_runs": src_runs,
                "test_runs": test_runs,
                "test_results": test_results,
            }
        )
    except Exception as e:
        return jsonify({"error": str(e)}), 400

