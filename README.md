# Environment Variables

- OPENPATCH_MODE one of ["development", "production", "testing"]

# Testing

```
docker-compose run --rm -e OPENPATCH_MODE="testing" runner-python flask test
```

# Coverage

```
docker-compose run --rm -e OPENPATCH_MODE="testing" runner-python python3 coverage_report.py
```

