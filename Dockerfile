FROM registry.gitlab.com/openpatch/flask-microservice-base:v3.7.0

RUN apt-get update && \
    apt-get -y upgrade

COPY "." "/var/www/app"

# Install python dependencies site-wide, since this is an isolated app in a container. We do not make use of a venv.
RUN pip3 install -r /var/www/app/requirements.txt
