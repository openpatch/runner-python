import unittest
import sys
import os
from coverage import Coverage
from openpatch_runner_python import create_app

environment = os.getenv("OPENPATCH_MODE", "development")
app = create_app(environment)


@app.cli.command()
def test():
    tests = unittest.TestLoader().discover("tests", pattern="test*.py")
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    sys.exit(1)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
