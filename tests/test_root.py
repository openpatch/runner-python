from tests.base_test import BaseTest


class RootTest(BaseTest):
    def test_endpoints(self):
        response = self.client.get("/endpoints")

        expected_payload = "/v1/run/post"

        self.assertEqual(response.get_json().get("payload"), expected_payload)

    def test_healthcheck(self):
        response = self.client.get("/healthcheck")

        self.assertEqual(response.get_json().get("status"), "healthy")
