from tests.base_test import BaseTest


class RoutesTest(BaseTest):
    def test_run_post_no_tests(self):
        json = {
            "sources": [
                {
                    "code": 'def print_hi():\n\treturn"Hi"',
                    "module": "my_module.hello_word",
                }
            ],
        }

        excepted_test_results = []

        response = self.client.post("/v1/run/post", json=json)

        self.assertEqual(response.get_json().get("test_results"), excepted_test_results)

    def test_run_post_failed_test(self):
        json = {
            "sources": [
                {
                    "code": 'def print_hi():\n\treturn"Hi"',
                    "module": "my_module.hello_word",
                }
            ],
            "tests": [
                {
                    "code": "import unittest\nfrom my_module.hello_word import print_hi\n\nclass TestHelloWorld(unittest.TestCase):\n\n\tdef test_print_hi(self):\n\t\tself.assertEqual(print_hi(), \"Hi\")\n\tdef test_print_hi_fail(self):\n\t\tself.assertEqual(print_hi(), 'Hi')",
                    "module": "test",
                }
            ],
        }

        excepted_test_results = [
            {
                "is_correct": True,
                "total_tests_failed": 0,
                "total_tests_run": 2,
                "values": [],
            }
        ]

        response = self.client.post("/v1/run/post", json=json)

        self.assertEqual(response.get_json().get("test_results"), excepted_test_results)

    def test_run_post_malformed(self):
        json = {
            "src": [
                {
                    "code": 'def print_hi():\n\treturn"Hi"',
                    "module": "my_module.hello_word",
                }
            ],
        }

        response = self.client.post("/v1/run/post", json=json)

        self.assertIn("error", response.get_json())
