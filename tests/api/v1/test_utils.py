import unittest
import os

from openpatch_runner_python.api.v1.utils import (
    convert_unittest_output,
    PythonToolchainFacade,
)


class TestUtils(unittest.TestCase):
    def test_convert_empty(self):
        output = ""

        result = convert_unittest_output(output)

        expected_result = {
            "error": "Could not parse unittest-output.",
            "error_msg": "Given unittest_output was either empty, malformed or the test did not compile at all.",
            "unittest_output": output,
        }

        self.assertEqual(result, expected_result)

    def test_convert_unittest_one_failure2(self):
        output = ".F\n======================================================================\nFAIL: test_print_hi_fail (tests.test.TestHelloWorld)\n----------------------------------------------------------------------\nTraceback (most recent call last):\n  File \"/tmp/flask_pid-6_1591297486896/tests/test.py\", line 9, in test_print_hi_fail\n    self.assertEqual(print_hi(), 'Hu')\nAssertionError: 'Hi' != 'Hu'\n- Hi\n+ Hu\n\n\n----------------------------------------------------------------------\nRan 2 tests in 0.000s\n\nFAILED (failures=1)\n"

        result = convert_unittest_output(output)

        expected_result = {
            "is_correct": False,
            "total_tests_run": 2,
            "total_tests_failed": 1,
            "values": [
                {
                    "module_name": "tests.test",
                    "class_name": "TestHelloWorld",
                    "method_name": "test_print_hi_fail",
                    "line": 9,
                    "assertion_error": "'Hi' != 'Hu'",
                }
            ],
        }

        self.assertEqual(result, expected_result)

    def test_convert_unittest_one_failure(self):
        output = """
F.
======================================================================
FAIL: test_test_me_fail (other_module.test_my_module.TestMyModule)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "other_module/test_my_module.py", line 11, in test_test_me_fail
    self.assertIn(test_me(), "U")
AssertionError: 'hi' not found in 'U'

----------------------------------------------------------------------
Ran 2 tests in 0.001s

FAILED (failures=1)
"""

        result = convert_unittest_output(output)

        expected_result = {
            "is_correct": False,
            "total_tests_run": 2,
            "total_tests_failed": 1,
            "values": [
                {
                    "module_name": "other_module.test_my_module",
                    "class_name": "TestMyModule",
                    "method_name": "test_test_me_fail",
                    "line": 11,
                    "assertion_error": "'hi' not found in 'U'",
                }
            ],
        }

        self.assertEqual(result, expected_result)

    def test_convert_unittest_no_tests(self):
        output = """
----------------------------------------------------------------------
Ran 0 tests in 0.000s

OK
"""
        result = convert_unittest_output(output)
        expected_result = {
            "error": "Could not parse unittest-output.",
            "error_msg": "No tests were provided!",
            "unittest_output": output,
        }

        self.assertEqual(result, expected_result)

    def test_convert_unittest_all_ok(self):
        output = """
..
----------------------------------------------------------------------
Ran 2 tests in 0.000s

OK
"""
        result = convert_unittest_output(output)
        expected_result = {
            "is_correct": True,
            "total_tests_failed": 0,
            "values": [],
            "total_tests_run": 2,
        }

        self.assertEqual(result, expected_result)


class TestPythonToolchainFacade(unittest.TestCase):
    def test_init(self):
        ptf = PythonToolchainFacade()
        self.assertTrue(os.path.exists(ptf._base_dir))
        self.assertTrue(os.path.exists(ptf._tests_dir))
        self.assertTrue(os.path.exists(ptf._sources_dir))

    def test_write_file_source(self):
        ptf = PythonToolchainFacade()
        module = "my_module.module"
        content = "print('hi')"
        ptf.write_file(module, content, "source")
        self.assertTrue(os.path.exists(ptf._sources_dir + "my_module/module.py"))

    def test_write_file_test(self):
        ptf = PythonToolchainFacade()
        module = "my_module.module"
        content = "print('hi')"
        ptf.write_file(module, content, "test")
        self.assertTrue(os.path.exists(ptf._tests_dir + "my_module/module.py"))

    def test_compile_py_compile_ok(self):
        ptf = PythonToolchainFacade()
        module = "my_module.module"
        content = "print('hi')"
        ptf.write_file(module, content, "source")
        response = ptf.compile_py_compile(module, "source")

        self.assertEqual(response["exitcode"], 0)
        self.assertEqual(response["stdout"], "")
        self.assertEqual(response["stderr"], "")

    def test_compile_py_compile_fail(self):
        ptf = PythonToolchainFacade()
        module = "my_module.module"
        content = "print('hi'"
        ptf.write_file(module, content, "source")
        response = ptf.compile_py_compile(module, "source")

        self.assertEqual(response["exitcode"], 1)
        self.assertEqual(response["stdout"], "")
        self.assertIn("SyntaxError", response["stderr"])

    def test_run_source(self):
        ptf = PythonToolchainFacade()
        module = "my_module.module"
        content = "print('hi')"
        ptf.write_file(module, content, "source")
        response = ptf.run(module, "source")

        self.assertEqual(response["exitcode"], 0)
        self.assertEqual(response["stdout"], "hi\n")
        self.assertEqual(response["stderr"], "")

    def test_run_unittest(self):
        ptf = PythonToolchainFacade()
        module = "my_module.module"
        content = "def get_hi():\n\treturn 'hi'"
        ptf.write_file(module, content, "source")

        test_module = "test_module"
        test_content = """
import unittest
from my_module.module import get_hi

class TestModule(unittest.TestCase):
    def test_get_hi(self):
        self.assertEqual(get_hi(), 'hi')
"""
        ptf.write_file(test_module, test_content, "test")
        response = ptf.run(test_module, "test")

        self.assertEqual(response["exitcode"], 0)
        self.assertEqual(response["stdout"], "")
        self.assertEqual(
            response["stderr"],
            """.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
""",
        )
