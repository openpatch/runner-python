from flask_testing import TestCase
from openpatch_runner_python import create_app


class BaseTest(TestCase):
    def create_app(self):
        return create_app("testing")

