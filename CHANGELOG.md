## [1.0.1](https://gitlab.com/openpatch/runner-python/compare/v1.0.0...v1.0.1) (2021-01-11)


### Bug Fixes

* endpoints remove api prefix ([690b703](https://gitlab.com/openpatch/runner-python/commit/690b70362732a43d9f1ab135b4a9ac60fa39b0b4))
* test ([8f24b11](https://gitlab.com/openpatch/runner-python/commit/8f24b115daf93fcdace4eeff71126a26dc8fab0d))
